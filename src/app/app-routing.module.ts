import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './core/layout/layout.component';

import { LoginComponent } from './pages/auth/login/login.component';
import { SignupComponent } from './pages/auth/signup/signup.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { ForgotPasswordComponent } from './pages/auth/forgot-password/forgot-password.component';
import { AuthGuard } from './core/guards/auth.guard';
import { RoleGuard } from './core/guards/role.guard';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: SignupComponent},
  {path: 'register/detail', component: RegisterComponent},
  {path: 'forgot-password', component: ForgotPasswordComponent},
  {
    path: '', component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: 'app/pages/course/course.module#CourseModule',
      },
      {
        path: 'dashboard',
        loadChildren: 'app/pages/dashboard/dashboard-statistics/dashboard-statistics.module#DashboardStatisticsModule',
        canActivate: [AuthGuard, RoleGuard],
        data: {roles: ['admin']},
        pathMatch: 'full'
      },
      {
        path: 'content',
        loadChildren: 'app/pages/content/content.module#ContentModule',
        data: {roles: ['admin']},
        canActivate: [AuthGuard, RoleGuard],
      },
      {
        path: 'inbox',
        loadChildren: 'app/pages/inbox/inbox.module#InboxModule'
      },
      {
        path: 'profile',
        loadChildren: 'app/pages/profile/profile.module#ProfileModule'
      }
    ]
  },
  {path: '**', redirectTo: 'courses'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
