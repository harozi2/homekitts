import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginModule } from './login/login.module';
import { RegisterModule } from './register/register.module';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { SignupModule } from './signup/signup.module';

@NgModule({
  imports: [
    CommonModule,
    LoginModule,
    RegisterModule,
    SignupModule,
    ForgotPasswordModule
  ],
  declarations: []
})
export class AuthModule { }
