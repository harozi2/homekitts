import { Component, OnInit } from '@angular/core';
import { ROUTE_TRANSITION } from '../../app.animation';

@Component({
  selector: 'vr-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class ContentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
