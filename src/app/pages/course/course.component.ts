import { Component, OnInit } from '@angular/core';
import { ROUTE_TRANSITION } from '../../app.animation';

@Component({
  selector: 'vr-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
  animations: [...ROUTE_TRANSITION],
  host: { '[@routeTransition]': '' }
})
export class CourseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
