import { AfterViewInit, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { CommonService } from '../../../shared/services/common.service';
import { IUser } from '../../../shared/interfaces/user.interface';

@Component({
  selector: 'vr-toolbar-user-button',
  templateUrl: './toolbar-user-button.component.html',
  styleUrls: ['./toolbar-user-button.component.scss']
})
export class ToolbarUserButtonComponent implements OnInit, AfterViewInit {

  isOpen: boolean;
  me: IUser;

  constructor(
    private route: Router,
    private afAuth: AngularFireAuth,
    private afDB: AngularFireDatabase,
    private cdr: ChangeDetectorRef,
    private commonService: CommonService
  ) { }

  ngOnInit() {
    this.getCurrentUserDetail();
  }

  ngAfterViewInit() {
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  onClickOutside() {
    this.isOpen = false;
  }

  async logout() {
    try {
      await this.afAuth.auth.signOut();
      this.commonService.storeUserInfo(null);
    } catch (e) {
      console.log(e);
    } finally {
      this.route.navigate(['/login']);
    }
  }

  async getCurrentUserDetail() {
    this.me = this.commonService.getUserInfo();
    if (!this.me) {
      this.me = {id: '', name: 'Unregistered', email: '', role: ''};
    }
  }
}
